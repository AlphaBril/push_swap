# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/20 15:08:30 by fldoucet          #+#    #+#              #
#    Updated: 2019/04/12 18:27:45 by fldoucet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME_PUSH		:=			push_swap
NAME_CHECK		:=			checker
CLIB_NAME		:=			libft.a

#==============================================================================#
#------------------------------------------------------------------------------#
#                               DIRECTORIES                                    #

SRC_DIR			:=			./srcs
INC_DIR			:=			./includes
OBJ_DIR			:=			./obj
LIB_DIR			:=			./libft

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  FILES                                       #

SRC_PUSH		:=		push_swap.c		\
						list_tool.c		\
						quicksort.c		\
						checker_ope.c	\
						push_tool.c		\
						direct_tool.c	\
						push_end.c		\
						push_ope.c		\
						push_sort.c		\
						quicksort_tools.c \
						brute_sort.c	\
						push_end_help.c	\
						checker_end.c	\
						min_max.c

SRC_CHECK		:=		checker.c		\
						checker_swap.c	\
						list_tool.c		\
						push_tool.c		\
						checker_ope.c	\
						push_end.c		\
						checker_visu.c	\
						checker_tool.c	\
						checker_inter.c \
						push_ope.c		\
						push_end_help.c \
						checker_end.c	

OBJ_PUSH		:=		$(addprefix $(OBJ_DIR)/,$(SRC_PUSH:.c=.o))
OBJ_CHECK		:=		$(addprefix $(OBJ_DIR)/,$(SRC_CHECK:.c=.o))
NB				:=		$(words $(SRC_PUSH) $(SRC_CHECK))
INDEX			:=		0

#==============================================================================#
#------------------------------------------------------------------------------#
#                            COMPILER & FLAGS                                  #

CC				:=			gcc
CFLAGS			:=			-Wall -Wextra -Werror -g
OFLAGS			:=			-pipe
CFLAGS			+=			$(OFLAGS)

#==============================================================================#
#------------------------------------------------------------------------------#
#                                LIBRARY                                       #

L_FT			:=			$(LIB_DIR)
CLIB			:=			$(addprefix $(LIB_DIR)/,$(CLIB_NAME))

INCXFOLDER=/usr/local/include/
LIBXFOLDER=/usr/local/lib/
LIBXFLAGS=-lmlx -framework OpenGL -framework AppKit

#==============================================================================#
#------------------------------------------------------------------------------#
#                                 RULES                                        #

all:					$(NAME_PUSH) $(NAME_CHECK)


$(NAME_PUSH):				$(OBJ_PUSH)
	@if [ ! -f $(CLIB) ]; then make -C $(L_FT) re --no-print-directory; fi
	@$(CC) $(OFLAGS) $(OBJ_PUSH) $(CLIB) -o $(NAME_PUSH)
	@printf '\033[33m[ 100%% ] %s\n\033[0m' "Compilation of $(NAME_PUSH) is done ---"


$(NAME_CHECK):				$(OBJ_CHECK)
	@if [ ! -f $(CLIB) ]; then make -C $(L_FT) re --no-print-directory; fi
	@$(CC) $(OFLAGS) $(OBJ_CHECK) $(CLIB) -o $(NAME_CHECK) -L $(LIBXFOLDER) $(LIBXFLAGS)
	@printf '\033[33m[ 100%% ] %s\n\033[0m' "Compilation of $(NAME_CHECK) is done ---"


$(OBJ_DIR)/%.o:			$(SRC_DIR)/%.c
	@mkdir -p $(OBJ_DIR)
	@$(eval DONE=$(shell echo $$(($(INDEX)*20/$(NB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEX)*100/$(NB)))))
	@$(eval TO_DO=$(shell echo "$@"))
	@$(CC) $(CFLAGS) -I $(INC_DIR) -I $(INCXFOLDER) -c $< -o $@
	@printf "[ %d%% ] %s :: %s        \r" $(PERCENT) $(NAME_PUSH) $@
	@$(eval INDEX=$(shell echo $$(($(INDEX)+1))))


clean:
	@make -C $(L_FT) clean --no-print-directory  
	@rm -rf $(OBJ_DIR)
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Clean of $(NAME_PUSH) ans $(NAME_CHECK) is done ---"


fclean: 				clean
	@rm -rf $(NAME_PUSH)
	@rm -rf $(NAME_CHECK)
	@make -C $(L_FT) fclean --no-print-directory
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Fclean of $(NAME_PUSH) and $(NAME_CHECK) is done ---"


re:						fclean all


.PHONY: clean
