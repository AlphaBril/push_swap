/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   direct_tool.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 16:05:32 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/15 15:48:02 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		ft_to_sort(t_pile **a, int pivot)
{
	t_pile	*tmp;

	tmp = *a;
	while (tmp)
	{
		if (tmp->val < pivot)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

int		ft_next_up(t_pile **a, int pivot)
{
	t_pile	*tmp;
	int		size;
	int		piv;

	tmp = *a;
	size = 0;
	piv = ft_next_down(a, pivot);
	while (tmp)
	{
		size++;
		tmp = tmp->next;
	}
	return (size - piv);
}

int		ft_next_down(t_pile **a, int pivot)
{
	t_pile	*tmp;
	int		i;

	tmp = *a;
	i = 0;
	while (tmp)
	{
		if (tmp->val < pivot)
			return (i);
		i++;
		tmp = tmp->next;
	}
	return (0);
}

int		ft_smallnext(t_pile **b)
{
	t_pile	*tmp;
	t_pile	*tmp2;
	int		biggest;
	int		next;

	tmp = *b;
	if (tmp)
		biggest = tmp->val;
	while (tmp)
	{
		if (tmp->val >= biggest)
		{
			biggest = tmp->val;
			tmp2 = *b;
			next = tmp2->val;
			while (tmp2)
			{
				if (tmp2->val >= next && tmp2->val < biggest)
					next = tmp2->val;
				tmp2 = tmp2->next;
			}
		}
		tmp = tmp->next;
	}
	return (next);
}

int		ft_pivot_down(t_pile **a, int pivot)
{
	t_pile	*tmp;
	int		i;
	int		piv;

	i = 0;
	piv = 0;
	tmp = *a;
	while (tmp)
	{
		if (tmp->val == pivot)
			piv = i;
		i++;
		tmp = tmp->next;
	}
	return (piv);
}
