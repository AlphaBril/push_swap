/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_sort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/27 15:47:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 14:44:38 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		ft_ishere(t_pile **a, int pivot)
{
	t_pile *tmp;

	tmp = *a;
	while (tmp)
	{
		if (tmp->val == pivot)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

void	ft_lastparseb(t_pile **a, t_pile **b, int pivot, t_push **swap)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (ft_to_sortb(b, pivot) == 1)
	{
		if ((*b)->val >= pivot)
		{
			ft_push_ope(a, b, swap, "pa");
			ft_check_sab(a, b, swap);
		}
		else if (ft_next_down(a, pivot) < ft_next_up(a, pivot))
			while ((*b)->val < pivot && ++i)
				ft_push_ope(a, b, swap, "rb");
		else if (ft_to_sortb(b, pivot == 1))
			while ((*b)->val < pivot && ++j)
				ft_push_ope(a, b, swap, "rrb");
	}
	while (i-- > 0)
		ft_push_ope(a, b, swap, "rrb");
	while (j-- > 0)
		ft_push_ope(a, b, swap, "rb");
}

void	ft_lastparsea(t_pile **a, t_pile **b, int pivot, t_push **swap)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (ft_to_sort(a, pivot) == 1)
	{
		if ((*a)->val < pivot)
		{
			ft_push_ope(a, b, swap, "pb");
			ft_check_sab(a, b, swap);
		}
		else if (ft_next_down(a, pivot) < ft_next_up(a, pivot))
			while ((*a)->val >= pivot && ++i)
				ft_push_ope(a, b, swap, "ra");
		else
			while ((*a)->val >= pivot && ++j)
				ft_push_ope(a, b, swap, "rra");
	}
	while (i-- > 0)
		ft_push_ope(a, b, swap, "rra");
	while (j-- > 0)
		ft_push_ope(a, b, swap, "ra");
}

void	ft_last_sort(t_pile **a, t_pile **b, t_pile **pivot, t_push **swap)
{
	t_pile	*piv;
	int		pivo;

	while (ft_check_pile(a) == -1)
	{
		if (!(piv = malloc(sizeof(t_pile))))
			ft_error(0);
		piv->next = NULL;
		piv->val = ft_mediane(a, ft_socket_size(a, ft_socket_enda(a)));
		ft_lst_push_pile(pivot, piv);
		ft_lastparsea(a, b, ft_mediane(a, ft_socket_size(a, ft_socket_enda(a)))
				, swap);
		ft_check_sab(a, b, swap);
	}
	pivo = ft_last_piv(pivot, b, 0);
	while (ft_ishere(b, pivo))
	{
		if (ft_socket_size(b, pivo) == 1)
			ft_push_ope(a, b, swap, "pa");
		else
			ft_lastparseb(a, b, ft_mediane(b, ft_socket_size(b, pivo)), swap);
		ft_check_sab(a, b, swap);
	}
	if (ft_check_pile(a) == -1 || (*b))
		ft_last_sort(a, b, pivot, swap);
}
