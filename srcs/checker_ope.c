/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_ope.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 16:14:53 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/15 15:40:39 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_pile_swap(t_pile **a, t_pile **b, int mode)
{
	t_pile *tmp;

	if (mode == 0 && *a && (*a)->next)
	{
		tmp = (*a)->next;
		(*a)->next = tmp->next;
		tmp->next = (*a);
		(*a) = tmp;
	}
	if (mode == 1 && *b && (*b)->next)
	{
		tmp = (*b)->next;
		(*b)->next = tmp->next;
		tmp->next = (*b);
		(*b) = tmp;
	}
	if (mode == 2)
	{
		ft_pile_swap(a, b, 0);
		ft_pile_swap(a, b, 1);
	}
}

void	ft_pile_push(t_pile **a, t_pile **b, int mode)
{
	t_pile *tmp;

	if (mode == 0 && *b)
	{
		tmp = *b;
		*b = (*b)->next;
		tmp->next = *a;
		*a = tmp;
	}
	if (mode == 1 && *a)
	{
		tmp = *a;
		*a = (*a)->next;
		tmp->next = *b;
		*b = tmp;
	}
	if (mode == 3)
	{
		ft_pile_rotate(a, b, 0);
		ft_pile_rotate(a, b, 1);
	}
}

void	ft_pile_rotate(t_pile **a, t_pile **b, int mode)
{
	t_pile *tmp;
	t_pile *tmp2;

	if (mode == 0 && *a && (*a)->next)
	{
		tmp = *a;
		tmp2 = tmp->next;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = *a;
		tmp->next->next = NULL;
		*a = tmp2;
	}
	if (mode == 1 && *b && (*b)->next)
	{
		tmp = *b;
		tmp2 = tmp->next;
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = *b;
		tmp->next->next = NULL;
		*b = tmp2;
	}
}

void	ft_pile_rev_rotate(t_pile **a, t_pile **b, int mode)
{
	t_pile *tmp;
	t_pile *tmp2;

	if (mode == 0 && *a && (*a)->next)
	{
		tmp = *a;
		while (tmp->next->next)
			tmp = tmp->next;
		tmp2 = tmp->next;
		tmp->next = NULL;
		tmp2->next = *a;
		*a = tmp2;
	}
	if (mode == 1 && *b && (*b)->next)
	{
		tmp = *b;
		while (tmp->next->next)
			tmp = tmp->next;
		tmp2 = tmp->next;
		tmp->next = NULL;
		tmp2->next = *b;
		*b = tmp2;
	}
}
