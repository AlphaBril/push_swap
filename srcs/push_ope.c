/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_ope.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/21 15:43:41 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 14:44:50 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		ft_smaller(t_pile **a)
{
	t_pile	*tmp;
	int		low;

	tmp = *a;
	low = tmp->val;
	while (tmp)
	{
		if (tmp->val > low)
			low = tmp->val;
		tmp = tmp->next;
	}
	return (low);
}

void	ft_push_swap(t_push **swap, char *str)
{
	t_push *new;

	if (swap == NULL)
		return ;
	else
	{
		if (!(new = malloc(sizeof(t_push))))
			return ;
		new->actio = ft_strdup(str);
		new->next = NULL;
		ft_lst_push_swap(swap, new);
	}
}

void	ft_push_ope_next2(t_pile **a, t_pile **b, t_push **swap, char *actio)
{
	if (ft_strcmp(actio, "rra") == 0)
	{
		ft_pile_rev_rotate(a, b, 0);
		ft_push_swap(swap, "rra");
	}
	if (ft_strcmp(actio, "rrb") == 0)
	{
		ft_pile_rev_rotate(a, b, 1);
		ft_push_swap(swap, "rrb");
	}
	if (ft_strcmp(actio, "rrr") == 0)
	{
		ft_pile_rev_rotate(a, b, 0);
		ft_pile_rev_rotate(a, b, 1);
		ft_push_swap(swap, "rrr");
	}
}

void	ft_push_ope_next(t_pile **a, t_pile **b, t_push **swap, char *actio)
{
	if (ft_strcmp(actio, "pb") == 0)
	{
		ft_pile_push(a, b, 1);
		ft_push_swap(swap, "pb");
	}
	if (ft_strcmp(actio, "ra") == 0)
	{
		ft_pile_rotate(a, b, 0);
		ft_push_swap(swap, "ra");
	}
	if (ft_strcmp(actio, "rb") == 0)
	{
		ft_pile_rotate(a, b, 1);
		ft_push_swap(swap, "rb");
	}
	if (ft_strcmp(actio, "rr") == 0)
	{
		ft_pile_rotate(a, b, 0);
		ft_pile_rotate(a, b, 1);
		ft_push_swap(swap, "rr");
	}
	ft_push_ope_next2(a, b, swap, actio);
}

void	ft_push_ope(t_pile **a, t_pile **b, t_push **swap, char *actio)
{
	if (ft_strcmp(actio, "sa") == 0)
	{
		ft_pile_swap(a, b, 0);
		ft_push_swap(swap, "sa");
	}
	if (ft_strcmp(actio, "sb") == 0)
	{
		ft_pile_swap(a, b, 1);
		ft_push_swap(swap, "sb");
	}
	if (ft_strcmp(actio, "ss") == 0)
	{
		ft_pile_swap(a, b, 2);
		ft_push_swap(swap, "ss");
	}
	if (ft_strcmp(actio, "pa") == 0)
	{
		ft_pile_push(a, b, 0);
		ft_push_swap(swap, "pa");
	}
	ft_push_ope_next(a, b, swap, actio);
}
