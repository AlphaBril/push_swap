/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   min_max.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 16:59:54 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/15 15:49:12 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		ft_slice(int size)
{
	int	ret;

	if (size < 50)
		ret = 1 + size / 10;
	else if (size < 100)
		ret = 2 + size / 10;
	else if (size < 150)
		ret = 8 + size / 100;
	else if (size < 300)
		ret = 12 + size / 100;
	else
		ret = 13 + size / 100;
	ret = (ret == 0) ? 1 : ret;
	return (ret);
}

int		ft_calc_minmax(t_pile **a, int size, int mode)
{
	t_pile	*tmp;
	int		tab[size];
	int		tmp2;
	int		i;

	i = 0;
	tmp = *a;
	while (tmp && i < size)
	{
		tab[i] = tmp->val;
		tmp = tmp->next;
		i++;
	}
	i = -1;
	while (++i < (size - 1))
	{
		if (tab[i] > tab[i + 1])
		{
			tmp2 = tab[i];
			tab[i] = tab[i + 1];
			tab[i + 1] = tmp2;
			i = 0;
		}
	}
	return (mode == 0 ? tab[ft_slice(size)] : tab[size - ft_slice(size) - 1]);
}

void	ft_push_back2(t_pile **a, t_pile **b, t_push **swap, int **data)
{
	if (ft_pivot_down(b, (*data)[1]) < ft_pile_size(b)
		- ft_pivot_down(b, (*data)[1]))
		if (ft_pivot_down(b, (*data)[2]) < ft_pivot_down(b, (*data)[1])
			&& (*data)[0] == 0)
		{
			(*data)[0] = 1;
			while ((*b)->val != (*data)[2])
				ft_push_ope(a, b, swap, "rb");
		}
		else
			while ((*b)->val != (*data)[1])
				ft_push_ope(a, b, swap, "rb");
	else
	{
		if (ft_pile_size(b) - ft_pivot_down(b, (*data)[2]) < ft_pile_size(b)
			- ft_pivot_down(b, (*data)[1]) && (*data)[0] == 0)
		{
			(*data)[0] = 1;
			while ((*b)->val != (*data)[2])
				ft_push_ope(a, b, swap, "rrb");
		}
		else
			while ((*b)->val != (*data)[1])
				ft_push_ope(a, b, swap, "rrb");
	}
}

void	ft_push_back(t_pile **a, t_pile **b, t_push **swap)
{
	int		next;
	int		actual;
	int		swaped;
	int		*data[3];

	swaped = 0;
	while (*b)
	{
		actual = ft_smaller(b);
		next = ft_smallnext(b);
		data[0] = &swaped;
		data[1] = &actual;
		data[2] = &next;
		ft_push_back2(a, b, swap, data);
		ft_push_ope(a, b, swap, "pa");
		if (swaped == 1)
			swaped = 2;
		else if (swaped == 2)
		{
			swaped = 0;
			ft_push_ope(a, b, swap, "sa");
		}
	}
}

void	ft_min_max(t_pile **a, t_pile **b, t_push **swap)
{
	int min;
	int max;

	while (*a)
	{
		min = ft_calc_minmax(a, ft_pile_size(a), 0);
		max = ft_calc_minmax(a, ft_pile_size(a), 1);
		if (ft_pile_size(a) == 1)
			min = (*a)->val;
		if ((*a)->val <= min)
			ft_push_ope(a, b, swap, "pb");
		else if ((*a)->val >= max)
		{
			ft_push_ope(a, b, swap, "pb");
			ft_push_ope(a, b, swap, "rb");
		}
		else
			ft_push_ope(a, b, swap, "ra");
	}
	ft_push_back(a, b, swap);
}
