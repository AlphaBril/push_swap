/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_tool.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/25 12:18:59 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/15 15:41:33 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_draw_text(t_mlx **new, int mode)
{
	if (mode == 2)
	{
		mlx_string_put((*new)->init, (*new)->win, 867, 508, 0xFF0000,
			"[FAILED]");
		mlx_put_image_to_window((*new)->init, (*new)->win, (*new)->fl, 920, 50);
	}
	else if (mode == 1)
	{
		mlx_string_put((*new)->init, (*new)->win, 867, 508, 0x00FF00,
			"[SORTED]");
		mlx_put_image_to_window((*new)->init, (*new)->win, (*new)->gg, 920, 50);
	}
	else
	{
		if ((*new)->pause == 0)
			mlx_string_put((*new)->init, (*new)->win, 860, 508, 0x0000FF,
				"[RUNNING..]");
		else
		{
			mlx_string_put((*new)->init, (*new)->win, 860, 500, 0x0000FF,
				"[PAUSED...]");
			mlx_string_put((*new)->init, (*new)->win, 820, 520, 0x0000FF,
				"Press -> for next move");
		}
	}
}

void	ft_draw_interface(t_mlx **new)
{
	int		x;
	int		y;

	x = 905;
	while (x++ < 910)
	{
		y = 40;
		while (y++ < 1040)
			if (y < 500 || y > 540)
				pixel_put((*new)->data, x, y, 0x000000);
	}
	x = 875;
	while (x++ < 940)
	{
		y = 1035;
		while (y++ < 1040)
			pixel_put((*new)->data, x, y, 0x000000);
	}
	x = 875;
	while (x++ < 940)
	{
		y = 40;
		while (y++ < 45)
			pixel_put((*new)->data, x, y, 0x000000);
	}
}

int		ft_pile_max(t_pile **a)
{
	t_pile	*tmp;
	int		ret;

	tmp = *a;
	ret = tmp->val;
	while (tmp)
	{
		if ((tmp->val < 0 ? tmp->val * -1 : tmp->val) > ret)
			ret = tmp->val;
		tmp = tmp->next;
	}
	return (ret);
}

void	ft_init_mlx(t_mlx **new, t_pile **a, t_pile **b, t_push **swap)
{
	int		bbp;
	int		s_l;
	int		endian;
	int		x[2];
	int		y[2];

	x[0] = 0;
	x[1] = 0;
	y[0] = 0;
	y[1] = 0;
	(*new)->init = mlx_init();
	(*new)->win = mlx_new_window((*new)->init, 1920, 1080,
		"Push Swap Visualizer (Better than Swap Push)");
	(*new)->pause = 0;
	(*new)->speed = 1;
	(*new)->image = mlx_new_image((*new)->init, 1920, 1080);
	(*new)->xmax = ft_pile_max(a);
	(*new)->ymax = ft_pixel_heigth(a, new);
	(*new)->nbr = 0;
	(*new)->a = a;
	(*new)->b = b;
	(*new)->data = mlx_get_data_addr((*new)->image, &bbp, &s_l, &endian);
	(*new)->gg = mlx_xpm_file_to_image((*new)->init, "./srcs/gg.xpm", x, y);
	(*new)->fl = mlx_xpm_file_to_image((*new)->init, "./srcs/fail.xpm", x, y);
	(*new)->start = swap;
}

int		ft_pixel_heigth(t_pile **a, t_mlx **new)
{
	double	ret;

	(*new)->col_bac = 0xADD8E6;
	(*new)->cl_ps = 0xFF0000;
	(*new)->cl_ng = 0x0000FF;
	ret = (1080 - 100) / ft_pile_size(a);
	if (ret < 1)
		return (1);
	if (ret > 50)
		return (50);
	return ((int)ret);
}
