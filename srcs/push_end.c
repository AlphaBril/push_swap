/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_end.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 18:07:27 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/15 15:49:25 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_free_pile(t_pile **to_free)
{
	t_pile *tmp;

	if (to_free)
	{
		while (*to_free)
		{
			tmp = *to_free;
			*to_free = (*to_free)->next;
			free(tmp);
		}
	}
}

void	ft_im_free(t_pile **a, t_pile **b, t_pile **pivot, t_push **swap)
{
	t_push *tmp;

	ft_free_pile(a);
	ft_free_pile(b);
	ft_free_pile(pivot);
	while (*swap)
	{
		tmp = *swap;
		*swap = (*swap)->next;
		if (tmp->actio)
			ft_strdel(&tmp->actio);
		free(tmp);
	}
}

void	ft_print_swap(t_push **swap, int mode, int start)
{
	t_push	*tmp;

	tmp = *swap;
	while (tmp)
	{
		if (ft_strcmp(tmp->actio, "pa") == 0 && start == 0 && mode == 1)
		{
			start = 1;
			while (ft_strcmp(tmp->actio, "pa") == 0)
				tmp = tmp->next;
		}
		if (ft_check_replacer(tmp))
		{
			ft_printf("%s\n", "rr");
			tmp = tmp->next;
		}
		else if (ft_check_replacerr(tmp))
		{
			ft_printf("%s\n", "rrr");
			tmp = tmp->next;
		}
		else
			ft_printf("%s\n", tmp->actio);
		tmp = tmp->next;
	}
}

void	ft_check_swap(t_push **swap, int mode)
{
	t_push *tmp;
	t_push *prev;
	t_push *next;

	tmp = *swap;
	prev = tmp;
	if (tmp && tmp->next)
		next = tmp->next;
	while (tmp)
	{
		next = tmp->next;
		if (ft_check_destroy(tmp) == 1 && mode == 0)
		{
			prev->next = next->next;
			ft_strdel(&tmp->actio);
			free(tmp);
			ft_strdel(&next->actio);
			free(next);
			tmp = *swap;
		}
		prev = tmp;
		tmp = tmp->next;
	}
	ft_print_swap(swap, mode, 0);
}
