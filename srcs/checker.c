/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 16:05:03 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:45:51 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_error(int error)
{
	if (error == 0)
		ft_printf("{red}%s{eoc}\n", "Error");
	if (error == 1)
		ft_printf("{red}%s{eoc}\n", "KO");
	exit(0);
}

void	ft_get_instruct(t_push **swap)
{
	t_push	*tmp;
	char	*line;
	char	**parse;
	int		i;

	i = 0;
	tmp = NULL;
	line = NULL;
	ft_swallow(0, &line);
	if ((ft_strlen(line) > 1 && ft_strchr(line, '\n') == NULL)
		|| ft_countword(line, 0, 0, '\n') != ft_nmatch(line, "*\n*"))
		ft_error(0);
	parse = ft_strsplit(line, '\n');
	while (parse && parse[i])
	{
		if (!(tmp = malloc(sizeof(t_push))))
			ft_error(0);
		tmp->actio = parse[i];
		tmp->next = NULL;
		ft_lst_push_swap(swap, tmp);
		i++;
	}
	free(parse);
	ft_strdel(&line);
}

void	ft_check_param(char **av, t_pile **a, int *visu)
{
	t_pile	*tmp;
	int		i;
	int		j;

	i = 0;
	if (ft_strcmp(av[0], "-v") == 0)
		*visu = 1;
	else
		i = -1;
	tmp = NULL;
	while (av[++i])
	{
		j = -1;
		while (av[i][++j])
			if (((av[i][j] < 48 || av[i][j] > 57) && av[i][j] != 45)
				|| ft_strcmp(av[i], "-") == 0 || ft_nmatch(av[i], "*-*") > 1
				|| ft_strlen(av[i]) > 11)
				ft_error(0);
		if (!(tmp = malloc(sizeof(t_pile))))
			ft_error(0);
		tmp->val = ft_atoll(av[i]);
		tmp->next = NULL;
		ft_lst_push_pile(a, tmp);
	}
}

void	ft_check_arg(char **av, t_pile **a, int *visu)
{
	char	**tab;
	int		i;
	int		j;

	j = 0;
	while (av[j])
	{
		tab = ft_strsplit(av[j], ' ');
		i = 0;
		if (tab[i])
			ft_check_param(tab, a, visu);
		while (tab[i])
		{
			ft_strdel(&tab[i]);
			i++;
		}
		free(tab);
		j++;
	}
}

int		main(int ac, char **av)
{
	t_push	*swap;
	t_pile	*a;
	t_pile	*b;
	int		visu;

	swap = NULL;
	a = NULL;
	b = NULL;
	visu = 0;
	if (ac != 1)
	{
		ft_check_arg(&av[1], &a, &visu);
		ft_get_instruct(&swap);
		ft_check_instruct(&swap, &a);
		ft_exec(&swap, &a, &b, visu);
		ft_check_list(&a, &b, &swap);
	}
	return (0);
}
