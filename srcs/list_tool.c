/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_tool.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 14:12:50 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/15 15:48:39 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void		ft_print_piles(t_pile *a, t_pile *b)
{
	ft_printf("/-------------------------------------------------------\\\n%w"
		, 1);
	ft_printf("| %15s%10s | %15s%10s |\n%w", "pile a", "", "pile b", "", 1);
	ft_printf("|-------------------------------------------------------|\n%w"
		, 1);
	while (a || b)
	{
		if (a && b)
			ft_printf("| %15d%10s | %15d%10s |\n%w", a->val, "", b->val, ""
				, 1);
		else if (a)
			ft_printf("| %15d%10s | %15s%10s |\n%w", a->val, "", "", "", 1);
		else if (b)
			ft_printf("| %15s%10s | %15d%10s |\n%w", "", "", b->val, "", 1);
		if (a)
			a = a->next;
		if (b)
			b = b->next;
	}
	ft_printf("\\_______________________________________________________/\n%w"
			, 1);
}

void		ft_print_instruct(t_push **swap)
{
	t_push *tmp;

	tmp = *swap;
	while (tmp)
	{
		ft_printf("{red}%s{eoc}\n", tmp->actio);
		tmp = tmp->next;
	}
}

void		ft_check_instruct(t_push **swap, t_pile **a)
{
	t_push *tmp;

	tmp = *swap;
	while (tmp)
	{
		if (ft_strcmp(tmp->actio, "sa") != 0 && ft_strcmp(tmp->actio, "sb")
			&& ft_strcmp(tmp->actio, "ss") != 0 && ft_strcmp(tmp->actio, "pa")
			&& ft_strcmp(tmp->actio, "pb") != 0 && ft_strcmp(tmp->actio, "ra")
			&& ft_strcmp(tmp->actio, "rb") != 0 && ft_strcmp(tmp->actio, "rr")
			&& ft_strcmp(tmp->actio, "rra") != 0 && ft_strcmp(tmp->actio, "rrb")
			&& ft_strcmp(tmp->actio, "rrr") != 0)
			ft_error(0);
		tmp = tmp->next;
	}
	ft_check_double(a);
}

void		ft_lst_push_swap(t_push **swap, t_push *new)
{
	t_push	*tmp;

	tmp = *swap;
	if (tmp == NULL)
		*swap = new;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}

void		ft_lst_push_pile(t_pile **swap, t_pile *new)
{
	t_pile	*tmp;

	tmp = *swap;
	if (tmp == NULL)
	{
		*swap = new;
		(*swap)->next = NULL;
	}
	else
	{
		while (tmp && tmp->next)
			tmp = tmp->next;
		tmp->next = new;
		tmp->next->next = NULL;
	}
}
