/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_visu.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/22 17:35:48 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:49:59 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	clear_pixels(char *str, int color)
{
	int		i;
	int		r;
	int		g;
	int		b;

	i = 0;
	r = color / 65536;
	g = (color / 256) % 256;
	b = color % 256;
	while (i < (1920 * 1080 * 4))
	{
		str[i + 2] = r;
		str[i + 1] = g;
		str[i + 0] = b;
		i = i + 4;
	}
}

void	pixel_put(char *str, int x, int y, int color)
{
	int		r;
	int		g;
	int		b;

	r = color / 65536;
	g = (color / 256) % 256;
	b = color % 256;
	if ((((x * 4) + 4 * 1920 * y) + 3) < (1920 * 1080 * 4) && x > 0 && y > 0
		&& x < 1920 && y < 1080)
	{
		str[((x * 4) + 4 * 1920 * y) + 2] = r;
		str[((x * 4) + 4 * 1920 * y) + 1] = g;
		str[((x * 4) + 4 * 1920 * y) + 0] = b;
	}
}

void	ft_draw_a(t_pile **a, t_mlx **new, int i, double tmp2)
{
	t_pile	*tmp;
	int		x;
	int		y;

	tmp = *a;
	while (tmp)
	{
		i = i + (*new)->ymax;
		y = i;
		while (y++ < i + (*new)->ymax)
		{
			x = tmp->val < 0 ? ((double)tmp->val * -1) / (*new)->xmax * 400
				+ 450 : (double)tmp->val / (*new)->xmax * 400 + 450;
			tmp2 = x;
			while (x-- > 50 + (850 - tmp2))
			{
				if (y == (i + (*new)->ymax) && (*new)->ymax != 1)
					pixel_put((*new)->data, x, y, 0x0);
				else
					tmp->val < 0 ? pixel_put((*new)->data, x, y, (*new)->cl_ng)
						: pixel_put((*new)->data, x, y, (*new)->cl_ps);
			}
		}
		tmp = tmp->next;
	}
}

void	ft_draw_b(t_pile **b, t_mlx **new, int i, double tmp2)
{
	t_pile	*tmp;
	double	x;
	int		y;

	tmp = *b;
	while (tmp)
	{
		i = i + (*new)->ymax;
		y = i;
		while (y++ < i + (*new)->ymax)
		{
			x = tmp->val < 0 ? ((double)tmp->val * -1) / (*new)->xmax * 400
				+ 1360 : ((double)tmp->val / (*new)->xmax * 400) + 1360;
			tmp2 = x;
			while (x-- > 1870 + (850 - tmp2))
			{
				if (y == (i + (*new)->ymax) && (*new)->ymax != 1)
					pixel_put((*new)->data, x, y, 0x0);
				else
					tmp->val < 0 ? pixel_put((*new)->data, x, y, (*new)->cl_ng)
						: pixel_put((*new)->data, x, y, (*new)->cl_ps);
			}
		}
		tmp = tmp->next;
	}
}

void	ft_draw_piles(t_pile **a, t_pile **b, t_mlx **new, int end)
{
	mlx_clear_window((*new)->init, (*new)->win);
	clear_pixels((*new)->data, (*new)->col_bac);
	ft_draw_a(a, new, 20, 0);
	ft_draw_b(b, new, 20, 0);
	ft_draw_interface(new);
	mlx_put_image_to_window((*new)->init, (*new)->win, (*new)->image, 0, 0);
	if (end == 1 && !(*b) && ft_check_pile(a) == 0)
		ft_draw_text(new, 1);
	else if (end == 1)
		ft_draw_text(new, 2);
	else
		ft_draw_text(new, 0);
	ft_draw_str(new);
	mlx_do_sync((*new)->init);
	mlx_hook((*new)->win, 2, 1L << 0, deal_key, new);
}
