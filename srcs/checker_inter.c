/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_inter.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/25 18:36:05 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:54:14 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_rev_op(char *str, t_pile **a, t_pile **b)
{
	if (ft_strcmp(str, "pa") == 0)
		ft_op("pb", a, b);
	if (ft_strcmp(str, "pb") == 0)
		ft_op("pa", a, b);
	if (ft_strcmp(str, "sa") == 0)
		ft_op("sa", a, b);
	if (ft_strcmp(str, "sb") == 0)
		ft_op("sb", a, b);
	if (ft_strcmp(str, "ss") == 0)
		ft_op("ss", a, b);
	if (ft_strcmp(str, "ra") == 0)
		ft_op("rra", a, b);
	if (ft_strcmp(str, "rra") == 0)
		ft_op("ra", a, b);
	if (ft_strcmp(str, "rb") == 0)
		ft_op("rrb", a, b);
	if (ft_strcmp(str, "rrb") == 0)
		ft_op("rb", a, b);
	if (ft_strcmp(str, "rrr") == 0)
		ft_op("rr", a, b);
	if (ft_strcmp(str, "rr") == 0)
		ft_op("rrr", a, b);
}

void	ft_rembobinate(t_mlx **new)
{
	t_push	*tmp;
	int		i;

	tmp = *((*new)->start);
	i = -1;
	if ((*new)->pause == 1 && (*new)->nbr > 0)
	{
		ft_rev_op(((*new)->prev)->actio, (*new)->a, (*new)->b);
		(*new)->action = ((*new)->prev)->actio;
		(*new)->nbr = (*new)->nbr - 1;
		while (++i < (*new)->nbr)
		{
			(*new)->prev = tmp;
			tmp = tmp->next;
		}
		if ((*new)->nbr == 0)
			(*new)->prev = *((*new)->start);
		(*new)->tmp = tmp;
	}
}

void	ft_pause(t_mlx **new)
{
	if ((*new)->pause == 0)
		(*new)->pause = 1;
	else
		(*new)->pause = 0;
}

void	ft_set_color(int key, t_mlx **new)
{
	if (key == 83)
	{
		(*new)->col_bac = 0xADD8E6;
		(*new)->cl_ps = 0xFF0000;
		(*new)->cl_ng = 0x0000FF;
	}
	if (key == 84)
	{
		(*new)->col_bac = 0x002B36;
		(*new)->cl_ps = 0x00B58B;
		(*new)->cl_ng = 0x005F5E;
	}
	if (key == 85)
	{
		(*new)->col_bac = 0x2F4F4F;
		(*new)->cl_ps = 0xFFD700;
		(*new)->cl_ng = 0xFF8C00;
	}
}

void	ft_draw_str(t_mlx **new)
{
	char *str;

	str = ft_strdup("[");
	str = ft_strjoin_free(str, ft_itoa((*new)->nbr), 0);
	str = ft_strjoin_free(str, "]", 1);
	mlx_string_put((*new)->init, (*new)->win, 880, 15, 0x0, str);
	ft_strdel(&str);
	if ((*new)->action)
		mlx_string_put((*new)->init, (*new)->win, 900, 1050, 0x0, (*new)->action);
}
