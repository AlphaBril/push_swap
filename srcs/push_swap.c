/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 16:03:47 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 14:44:11 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_print(t_push *swap)
{
	while (swap)
	{
		ft_printf("%s\n", swap->actio);
		swap = swap->next;
	}
}

void	ft_error(int error)
{
	if (error == 0)
		ft_printf("Error\n");
	exit(0);
}

void	ft_check_param(char **av, t_pile **a)
{
	t_pile	*tmp;
	int		i;
	int		j;

	i = 0;
	tmp = NULL;
	while (av[i])
	{
		j = -1;
		while (av[i][++j])
			if (((av[i][j] < 48 || av[i][j] > 57) && av[i][j] != 45)
				|| ft_strcmp(av[i], "-") == 0 || ft_nmatch(av[i], "*-*") > 1
				|| ft_strlen(av[i]) > 11)
				ft_error(0);
		if (!(tmp = malloc(sizeof(t_pile))))
			ft_error(0);
		tmp->val = ft_atoll(av[i]);
		tmp->next = NULL;
		ft_lst_push_pile(a, tmp);
		i++;
	}
	ft_check_double(a);
}

void	ft_check_arg(char **av, t_pile **a)
{
	char	**tab;
	int		i;
	int		j;

	j = 0;
	while (av[j])
	{
		tab = ft_strsplit(av[j], ' ');
		ft_check_param(tab, a);
		i = 0;
		while (tab[i])
		{
			ft_strdel(&tab[i]);
			i++;
		}
		free(tab);
		j++;
	}
}

int		main(int ac, char **av)
{
	t_push	*swap;
	t_pile	*a;
	t_pile	*b;
	t_pile	*pivot;

	swap = NULL;
	a = NULL;
	b = NULL;
	pivot = NULL;
	if (ac != 1)
	{
		ft_check_arg(&av[1], &a);
		ft_quicksort(&a, &b, &pivot, &swap);
		if (ft_pile_size(&a) <= 5)
			ft_check_swap(&swap, 1);
		else
			ft_check_swap(&swap, 0);
		ft_im_free(&a, &b, &pivot, &swap);
	}
	return (0);
}
