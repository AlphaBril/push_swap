/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_end_help.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 17:37:30 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 14:46:52 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_check_sab(t_pile **a, t_pile **b, t_push **swap)
{
	if ((*a) && (*a)->next && (*a)->val > (*a)->next->val)
		ft_push_ope(a, b, swap, "sa");
	if ((*b) && (*b)->next && (*b)->val < (*b)->next->val)
		ft_push_ope(a, b, swap, "sb");
}

int		ft_check_replacerr(t_push *tmp)
{
	if (tmp->next && ((ft_strcmp(tmp->actio, "rra") == 0
		&& ft_strcmp(tmp->next->actio, "rrb") == 0)
		|| (ft_strcmp(tmp->actio, "rrb") == 0
		&& ft_strcmp(tmp->next->actio, "rra") == 0)))
		return (1);
	return (0);
}

int		ft_check_replacer(t_push *tmp)
{
	if (tmp->next && ((ft_strcmp(tmp->actio, "ra") == 0
		&& ft_strcmp(tmp->next->actio, "rb") == 0)
		|| (ft_strcmp(tmp->actio, "rb") == 0
		&& ft_strcmp(tmp->next->actio, "ra") == 0)))
		return (1);
	return (0);
}

int		ft_check_destroy(t_push *tmp)
{
	if (tmp->next && ((ft_strcmp(tmp->actio, "pa") == 0
				&& ft_strcmp(tmp->next->actio, "pb") == 0)
			|| (ft_strcmp(tmp->actio, "pb") == 0
				&& ft_strcmp(tmp->next->actio, "pa") == 0)
			|| (ft_strcmp(tmp->actio, "ra") == 0
				&& ft_strcmp(tmp->next->actio, "rra") == 0)
			|| (ft_strcmp(tmp->actio, "rra") == 0
				&& ft_strcmp(tmp->next->actio, "ra") == 0)
			|| (ft_strcmp(tmp->actio, "rb") == 0
				&& ft_strcmp(tmp->next->actio, "rrb") == 0)
			|| (ft_strcmp(tmp->actio, "rrb") == 0
				&& ft_strcmp(tmp->next->actio, "rb") == 0)))
		return (1);
	return (0);
}
