/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_tool.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/19 13:48:28 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 14:44:03 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_del_swap(t_push **swap)
{
	t_push *tmp;
	t_push *prev;

	tmp = *swap;
	prev = *swap;
	while (tmp->next)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	ft_strdel(&tmp->actio);
	free(tmp);
	prev->next = NULL;
}

int		ft_swap_size(t_push **swap)
{
	t_push	*tmp;
	int		i;

	i = 0;
	tmp = *swap;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

int		ft_check_pile(t_pile **a)
{
	t_pile *tmp;

	tmp = *a;
	while (tmp && tmp->next)
	{
		if (tmp->val > tmp->next->val)
			return (-1);
		tmp = tmp->next;
	}
	return (0);
}

int		ft_pile_size(t_pile **to_size)
{
	t_pile	*tmp;
	int		i;

	i = 0;
	tmp = *to_size;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

int		ft_mediane(t_pile **a, int size)
{
	t_pile	*tmp;
	int		tab[size];
	int		tmp2;
	int		i;

	i = 0;
	tmp = *a;
	while (tmp && i < size)
	{
		tab[i] = tmp->val;
		tmp = tmp->next;
		i++;
	}
	i = -1;
	while (++i < (size - 1))
	{
		if (tab[i] > tab[i + 1])
		{
			tmp2 = tab[i];
			tab[i] = tab[i + 1];
			tab[i + 1] = tmp2;
			i = 0;
		}
	}
	return ((size % 2) == 0 ? tab[(size / 2)] : tab[(size / 2) + 1]);
}
