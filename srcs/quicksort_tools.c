/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quicksort_tools.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/29 18:35:04 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 14:43:47 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		ft_min(t_pile **a)
{
	t_pile	*tmp;
	int		min;

	tmp = *a;
	if (tmp)
		min = tmp->val;
	while (tmp)
	{
		if (tmp->val < min)
			min = tmp->val;
		tmp = tmp->next;
	}
	return (min);
}

int		ft_to_sortb(t_pile **b, int pivot)
{
	t_pile	*tmp;

	tmp = *b;
	while (tmp)
	{
		if (tmp->val >= pivot)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

int		ft_socket_size(t_pile **socket, int end)
{
	t_pile	*tmp;
	int		size;

	tmp = *socket;
	size = 0;
	while (tmp && tmp->val != end)
	{
		size++;
		tmp = tmp->next;
	}
	return (size + 1);
}

int		ft_socket_enda(t_pile **socket)
{
	t_pile	*tmp;
	int		end;

	tmp = *socket;
	end = tmp->val;
	while (tmp->next)
	{
		if (tmp->val > tmp->next->val)
			end = tmp->next->val;
		tmp = tmp->next;
	}
	return (end);
}

int		ft_last_piv(t_pile **pivot, t_pile **b, int check)
{
	t_pile	*pile;
	t_pile	*tmp;
	int		ret;

	pile = *b;
	ret = ft_min(pivot);
	while (pile)
	{
		tmp = *pivot;
		while (tmp)
		{
			if (tmp->val == pile->val && tmp->val >= ret)
			{
				check = 1;
				ret = tmp->val;
			}
			tmp = tmp->next;
		}
		pile = pile->next;
	}
	if (check == 1)
		return (ret);
	else
		return (ft_min(b));
}
