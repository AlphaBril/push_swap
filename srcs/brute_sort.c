/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   brute_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/04 11:50:51 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/15 15:38:49 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void		ft_op(char *actio, t_pile **a, t_pile **b)
{
	ft_strcmp(actio, "sa") == 0 ? ft_pile_swap(a, b, 0) : NULL;
	ft_strcmp(actio, "sb") == 0 ? ft_pile_swap(a, b, 1) : NULL;
	ft_strcmp(actio, "ss") == 0 ? ft_pile_swap(a, b, 2) : NULL;
	ft_strcmp(actio, "pa") == 0 ? ft_pile_push(a, b, 0) : NULL;
	ft_strcmp(actio, "pb") == 0 ? ft_pile_push(a, b, 1) : NULL;
	ft_strcmp(actio, "ra") == 0 ? ft_pile_rotate(a, b, 0) : NULL;
	ft_strcmp(actio, "rb") == 0 ? ft_pile_rotate(a, b, 1) : NULL;
	ft_strcmp(actio, "rra") == 0 ? ft_pile_rev_rotate(a, b, 0) : NULL;
	ft_strcmp(actio, "rrb") == 0 ? ft_pile_rev_rotate(a, b, 1) : NULL;
	if (ft_strcmp(actio, "rr") == 0)
	{
		ft_pile_rotate(a, b, 0);
		ft_pile_rotate(a, b, 1);
	}
	if (ft_strcmp(actio, "rrr") == 0)
	{
		ft_pile_rev_rotate(a, b, 0);
		ft_pile_rev_rotate(a, b, 1);
	}
}

void		ft_init_tab(char **tab)
{
	tab[0] = "pa";
	tab[1] = "pb";
	tab[2] = "ra";
	tab[3] = "rb";
	tab[4] = "rra";
	tab[5] = "rrb";
	tab[6] = "sa";
	tab[7] = "sb";
	tab[8] = "rr";
	tab[9] = "rrr";
}

void		ft_cp_pile(t_pile **dest, t_pile **src)
{
	t_pile *tmp;
	t_pile *tmp2;

	tmp = *src;
	while (tmp)
	{
		if (!(tmp2 = malloc(sizeof(t_pile))))
			ft_error(0);
		tmp2->val = tmp->val;
		tmp2->next = NULL;
		ft_lst_push_pile(dest, tmp2);
		tmp = tmp->next;
	}
}

int			ft_check_brute_force(t_pile **a, t_push **swap)
{
	t_pile	*ca;
	t_pile	*cb;
	t_push	*tmp;

	ca = NULL;
	cb = NULL;
	ft_cp_pile(&ca, a);
	tmp = *swap;
	while (tmp)
	{
		ft_op(tmp->actio, &ca, &cb);
		tmp = tmp->next;
	}
	if (ft_check_pile(&ca) == 0 && !cb)
	{
		ft_free_pile(&ca);
		ft_free_pile(&cb);
		return (1);
	}
	ft_free_pile(&ca);
	ft_free_pile(&cb);
	return (0);
}

int			ft_brute_sort(t_pile **a, t_pile **b, t_push **swap)
{
	char	*tab[10];
	int		i;

	i = 0;
	ft_init_tab(tab);
	if (ft_check_brute_force(a, swap) == 1)
		return (1);
	if (ft_swap_size(swap) > 12)
		return (0);
	while (i < 10)
	{
		ft_push_swap(swap, tab[i]);
		if (ft_brute_sort(a, b, swap) == 1)
			return (1);
		ft_del_swap(swap);
		i++;
	}
	return (0);
}
