/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quicksort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 14:13:25 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 14:43:54 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int		ft_pivot(t_pile **a, t_pile **pivot)
{
	t_pile	*tmp;
	t_pile	*piv;

	tmp = *a;
	if (!(piv = malloc(sizeof(t_pile))))
		ft_error(0);
	piv->next = NULL;
	while (tmp)
	{
		if (tmp->val == ft_mediane(a, ft_pile_size(a)))
		{
			piv->val = tmp->val;
			ft_lst_push_pile(pivot, piv);
			return (tmp->val);
		}
		tmp = tmp->next;
	}
	return (0);
}

void	ft_quickparse(t_pile **a, t_pile **b, int pivot, t_push **swap)
{
	while ((*a) && ft_to_sort(a, pivot) == 1)
	{
		if ((*a)->val < pivot)
			ft_push_ope(a, b, swap, "pb");
		else if (ft_next_down(a, pivot) < ft_next_up(a, pivot))
			while ((*a)->val >= pivot)
				ft_push_ope(a, b, swap, "ra");
		else
			while ((*a)->val >= pivot)
				ft_push_ope(a, b, swap, "rra");
	}
}

void	ft_quicksort(t_pile **a, t_pile **b, t_pile **pivot, t_push **swap)
{
	int		piv;

	if (ft_pile_size(a) <= 5)
		ft_brute_sort(a, b, swap);
	else if (ft_pile_size(a) <= 600 && ft_check_pile(a) == -1)
		ft_min_max(a, b, swap);
	else
	{
		while (ft_check_pile(a) == -1)
		{
			piv = ft_pivot(a, pivot);
			ft_quickparse(a, b, piv, swap);
		}
		ft_last_sort(a, b, pivot, swap);
	}
}
