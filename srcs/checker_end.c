/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_end.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 14:37:07 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:26:08 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void		ft_check_double(t_pile **a)
{
	t_pile	*tmp;
	t_pile	*tmp2;
	int		i;

	tmp = *a;
	while (tmp)
	{
		tmp2 = *a;
		i = 0;
		while (tmp2)
		{
			if (tmp2->val == tmp->val)
				i++;
			tmp2 = tmp2->next;
		}
		if (i > 1 || tmp->val > 2147483647 || tmp->val < -2147483648)
			ft_error(0);
		tmp = tmp->next;
	}
}
