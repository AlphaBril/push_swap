/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker_swap.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 14:02:33 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:47:41 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_op(char *actio, t_pile **a, t_pile **b)
{
	ft_strcmp(actio, "sa") == 0 ? ft_pile_swap(a, b, 0) : NULL;
	ft_strcmp(actio, "sb") == 0 ? ft_pile_swap(a, b, 1) : NULL;
	ft_strcmp(actio, "ss") == 0 ? ft_pile_swap(a, b, 2) : NULL;
	ft_strcmp(actio, "pa") == 0 ? ft_pile_push(a, b, 0) : NULL;
	ft_strcmp(actio, "pb") == 0 ? ft_pile_push(a, b, 1) : NULL;
	ft_strcmp(actio, "ra") == 0 ? ft_pile_rotate(a, b, 0) : NULL;
	ft_strcmp(actio, "rb") == 0 ? ft_pile_rotate(a, b, 1) : NULL;
	ft_strcmp(actio, "rra") == 0 ? ft_pile_rev_rotate(a, b, 0) : NULL;
	ft_strcmp(actio, "rrb") == 0 ? ft_pile_rev_rotate(a, b, 1) : NULL;
	if (ft_strcmp(actio, "rr") == 0)
	{
		ft_pile_rotate(a, b, 0);
		ft_pile_rotate(a, b, 1);
	}
	if (ft_strcmp(actio, "rrr") == 0)
	{
		ft_pile_rev_rotate(a, b, 0);
		ft_pile_rev_rotate(a, b, 1);
	}
}

int		deal_key(int key, void *arg)
{
	t_mlx **new;

	new = arg;
	if (key == 53)
		exit(0);
	if (key == 49)
		ft_pause(new);
	if (key == 124)
	{
		if ((*new)->pause == 1 && (*new)->tmp)
		{
			ft_op(((*new)->tmp)->actio, (*new)->a, (*new)->b);
			(*new)->nbr = (*new)->nbr + 1;
			(*new)->action = ((*new)->tmp)->actio;
			(*new)->prev = (*new)->tmp;
			(*new)->tmp = ((*new)->tmp)->next;
		}
	}
	if (key == 123)
		ft_rembobinate(new);
	if (key == 83 || key == 84 || key == 85)
		ft_set_color(key, new);
	return (0);
}

void	ft_exec_mlx(t_push **swap, t_pile **a, t_pile **b)
{
	t_mlx	*new;
	t_push	*tmp;

	if (!(new = malloc(sizeof(t_mlx))))
		return ;
	ft_init_mlx(&new, a, b, swap);
	tmp = *swap;
	while (tmp)
	{
		if (new->pause == 0)
		{
			ft_op(tmp->actio, a, b);
			new->action = tmp->actio;
			new->nbr = new->nbr + 1;
			new->prev = tmp;
			tmp = tmp->next;
			new->tmp = tmp;
		}
		tmp = new->tmp;
		ft_draw_piles(a, b, &new, 0);
	}
	ft_draw_piles(a, b, &new, 1);
	mlx_hook(new->win, 2, 1L << 0, deal_key, new);
	mlx_loop(new->init);
}

void	ft_exec(t_push **swap, t_pile **a, t_pile **b, int visu)
{
	t_push	*tmp;

	if (visu == 1 && *a)
		ft_exec_mlx(swap, a, b);
	else
	{
		tmp = *swap;
		while (tmp)
		{
			ft_op(tmp->actio, a, b);
			tmp = tmp->next;
		}
	}
}

int		ft_check_list(t_pile **a, t_pile **b, t_push **swap)
{
	t_pile *tmp;

	tmp = *a;
	while (tmp && tmp->next)
	{
		if (tmp->val > tmp->next->val)
		{
			ft_printf("%s\n", "KO");
			return (0);
		}
		tmp = tmp->next;
	}
	if (*b)
		ft_printf("%s\n", "KO");
	else
		ft_printf("%s\n", "OK");
	ft_im_free(a, b, NULL, swap);
	return (1);
}
