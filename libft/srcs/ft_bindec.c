/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bindec.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:38 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/30 19:14:59 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int	ft_power(int nb, int pow)
{
	int ret;

	ret = nb;
	if (pow == 0)
		return (1);
	while (pow > 1)
	{
		ret = ret * nb;
		pow--;
	}
	return (ret);
}

int			ft_bindec(char *bin)
{
	int		ret;
	int		i;
	int		j;

	j = 0;
	i = ft_strlen(bin) - 1;
	ret = 0;
	while (bin[i])
	{
		ret = ret + ((bin[i] - 48) * ft_power(2, j));
		j++;
		i--;
	}
	return (ret);
}
