/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_char.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:16:01 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int				ft_strlen_swap(t_lprintf *list)
{
	int		i;

	i = 0;
	while ((list->to_swap)[i])
		i++;
	if (ft_strchr(list->conversion, 'c'))
		if (list->arg == NULL)
			i++;
	if (ft_strchr(list->conversion, 'C'))
		if (ft_strlen(list->to_swap) == 0)
			i++;
	return (i);
}

void			handle_char(t_lprintf *list)
{
	unsigned char	*ret;

	ret = NULL;
	if (ft_strchr(list->conversion, 'c'))
	{
		if (!(ret = malloc(sizeof(unsigned char) * 2)))
			return ;
		if (list->modifier && ft_strcmp(list->modifier, "l") == 0)
		{
			if ((long)list->arg < 0 || (long)list->arg > 255)
			{
				list->error = 1;
				ret[0] = '\0';
			}
			else
				ret[0] = (long)list->arg;
		}
		else
			ret[0] = (unsigned char)list->arg;
		ret[1] = '\0';
	}
	else if (ft_strchr(list->conversion, 'C'))
		ret = handle_unicode((wchar_t)list->arg);
	list->to_swap = (char *)ret;
}
