/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:42 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:26:36 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	if ((s1[0] == '\0' && s2[0] == '\0') || n <= 0)
		return (0);
	while (*s1 && n > 0)
	{
		if (*s1 != *s2)
			return (*s1 < 0 ? 1 : *s1 - *s2);
		s1++;
		s2++;
		n--;
	}
	if (n == 0)
		return (0);
	if (*s2)
		return (-*s2);
	return (0);
}
