/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:41 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:19:05 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*dst_tmp;
	char	*src_tmp;
	size_t	x;

	dst_tmp = (char*)dst;
	src_tmp = (char*)src;
	x = -1;
	if (src_tmp < dst_tmp)
		while ((int)--len >= 0)
			dst_tmp[len] = src_tmp[len];
	else
		while (++x < len)
			dst_tmp[x] = src_tmp[x];
	return (dst);
}
