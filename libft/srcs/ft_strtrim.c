/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:28:52 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int	ft_isspace(char c)
{
	return ((c == '\t' || c == '\n' || c == ' '));
}

char		*ft_strtrim(char const *s)
{
	char	*tmp;
	size_t	x;
	size_t	len_tmp;

	x = 0;
	if (!s)
		return (NULL);
	len_tmp = ft_strlen(s);
	while (ft_isspace((int)s[x]))
		x++;
	while (ft_isspace((int)s[len_tmp - 1]))
		len_tmp--;
	if (x == ft_strlen(s))
	{
		tmp = ft_strsub(s, 0, 0);
		return (tmp);
	}
	if (!(tmp = ft_strsub(s, x, (len_tmp - x))))
		return (NULL);
	return (tmp);
}
