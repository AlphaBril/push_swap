/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstqueue.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:40 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:17:05 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_lstqueue(t_list **alst, t_list *new)
{
	t_list	*temp;

	temp = *alst;
	if (temp->content == NULL)
		*alst = ft_lstnew(new->content, new->content_size);
	else
	{
		while (temp->next)
			temp = temp->next;
		temp->next = ft_lstnew(new->content, new->content_size);
	}
}
