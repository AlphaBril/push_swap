/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_hexa_long.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:44 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:17:19 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static void	join_free_loop(char **ret, int length, t_lprintf **list)
{
	int	i;

	i = ft_strchr((*list)->special, '0') ? 1 : -1;
	while (++i < length)
		*ret = ft_strjoin_free(*ret, "0", 1);
}

static char	*compose_arg(t_lprintf **list, u_int64_t arg,
	int precision, int field)
{
	char	*ret;
	char	capital;
	char	*ox;
	char	*arglength;

	ret = NULL;
	capital = ft_strchr((*list)->conversion, 'x') ? 'a' : 'A';
	ox = capital == 'A' ? "0X" : "0x";
	arglength = ft_utoa_base(arg, 16, capital);
	if (ft_strchr((*list)->special, '#') != NULL)
		ret = ft_strjoin_free(ret, ox, 1);
	if (ft_strchr((*list)->special, '0') != NULL && precision == 0
		&& ft_strchr((*list)->special, '-') == NULL)
		join_free_loop(&ret, field - (int)ft_strlen_pro(arglength), list);
	else if (precision > 0)
		join_free_loop(&ret, precision - (int)ft_strlen_pro(arglength), list);
	if (ft_strchr((*list)->special, '#') == NULL
		&& ft_strchr((*list)->special, '0') != NULL)
		ret = ft_strjoin_free(ret, "00", 1);
	ret = ft_strjoin_free(ret, ft_utoa_base(arg, 16, capital), 0);
	ft_strdel(&arglength);
	return (ret);
}

void		handle_hexa_long(t_lprintf *list)
{
	int		precision;
	int		field;
	char	*arg;

	precision = list->dig_pos_pre != NULL ? ft_atoi(list->dig_pos_pre) : 0;
	field = list->field != NULL ? ft_atoi(list->field) : 0;
	arg = compose_arg(&list, (u_int64_t)list->arg, precision, field);
	if (ft_strchr(list->precision, '.') && precision == 0
		&& (u_int64_t)list->arg == 0)
		arg = ft_strsub_free(arg, ft_strlen_pro(arg), ft_strlen_pro(arg));
	if (list->special != NULL && ft_strchr(list->special, '-') != NULL)
		padd_digit(&list, arg, field, 1);
	else
		padd_digit(&list, arg, field, 0);
}
