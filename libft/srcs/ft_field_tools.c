/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_field_tools.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:39 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:12:30 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	print_field(int field, t_lprintf **list)
{
	int	arg;
	int	i;

	field = field < 0 ? 0 : field;
	arg = (int)(*list)->arg;
	if (ft_matcha((*list)->special, "+") && arg > 0
		&& !ft_matcha((*list)->conversion, "ouxX"))
		i = 1;
	else if (ft_matcha((*list)->special, " ") && field == 0 && arg > 0
		&& !ft_matcha((*list)->conversion, "o"))
		i = -1;
	else
		i = 0;
	while (i++ < field)
		(*list)->to_swap = ft_strjoin_free((*list)->to_swap, " ", 1);
}
