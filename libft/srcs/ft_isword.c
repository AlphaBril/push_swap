/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isword.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:39 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:14:14 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isword(int i, const char *s, char c)
{
	return ((i == 0 && s[i] != c) || (s[i] != c && s[i - 1] == c));
}
