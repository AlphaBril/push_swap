/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_long.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:39 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/30 19:16:06 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char			*ft_itoa_long(long long int n)
{
	int			i;
	long long	nb;
	int			neg;
	char		*res;

	if (!(res = (char*)malloc(sizeof(char)
		* (ft_intlen((long long int)n) + 1))))
		return (NULL);
	neg = n < 0;
	i = 0;
	ft_bzero(res, (ft_intlen(n) + 1));
	nb = n;
	if (nb == 0)
		res[i] = '0';
	while (nb)
	{
		res[i] = (char)ft_labs(nb % 10) + 48;
		nb = ft_labs(nb / 10);
		i++;
	}
	if (neg)
		res[i] = '-';
	ft_strrev(res);
	res[ft_intlen(n) + 1] = '\0';
	return (res);
}
