/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dneg.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:39 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:11:38 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int	ft_dneg(double val)
{
	char			*ret;
	unsigned		idx;
	unsigned char	*ptr;

	ptr = (unsigned char*)&val;
	idx = 8 * sizeof(val);
	ret = NULL;
	while (idx)
	{
		ret = ptr[idx / 8] & (1u << (idx % 8))
			? ft_strjoin_free(ret, "1", 1) : ft_strjoin_free(ret, "0", 1);
		idx--;
	}
	if (ret[1] == '1')
	{
		ft_strdel(&ret);
		return (1);
	}
	else
	{
		ft_strdel(&ret);
		return (0);
	}
}
