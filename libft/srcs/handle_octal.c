/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_octal.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:44 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:18:11 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static void	join_free_loop(char **ret, int length, t_lprintf **list)
{
	int	i;

	i = ft_strchr((*list)->special, '0') ? 1 : -1;
	while (++i < length)
		*ret = ft_strjoin_free(*ret, "0", 1);
}

static char	*compose_arg(t_lprintf **list, int precision,
	int field, int islong)
{
	char	*ret;
	char	capital;
	char	*ox;
	char	*arglength;

	ret = NULL;
	capital = ft_strchr((*list)->conversion, 'x') ? 'a' : 'A';
	ox = "0";
	arglength = ft_utoa_base(islong
			? (u_int64_t)(*list)->arg : (unsigned int)(*list)->arg, 8, capital);
	if (ft_strchr((*list)->special, '#') != NULL && (int)(*list)->arg != 0)
		ret = ft_strjoin_free(ret, ox, 1);
	if (ft_strchr((*list)->special, '0') != NULL && precision == 0
		&& ft_strchr((*list)->special, '-') == NULL)
		join_free_loop(&ret, field - (int)ft_strlen_pro(arglength), list);
	else if (precision > 0)
		join_free_loop(&ret, precision - (int)ft_strlen_pro(arglength), list);
	if (ft_strchr((*list)->special, '#') == NULL
		&& ft_strchr((*list)->special, '0') != NULL)
		ret = ft_strjoin_free(ret, "00", 1);
	ret = ft_strjoin_free(ret, ft_utoa_base(islong
				? (u_int64_t)(*list)->arg
				: (unsigned int)(*list)->arg, 8, capital), 0);
	ft_strdel(&arglength);
	return (ret);
}

void		handle_octal(t_lprintf *list)
{
	int		precision;
	int		field;
	char	*arg;

	precision = list->dig_pos_pre != NULL ? ft_atoi(list->dig_pos_pre) : 0;
	field = list->field != NULL ? ft_atoi(list->field) : 0;
	if (list->modifier
		&& (ft_match(list->modifier, "l") || ft_match(list->modifier, "ll")))
		arg = compose_arg(&list, precision, field, 1);
	else
		arg = compose_arg(&list, precision, field, 0);
	if (ft_strchr(list->precision, '.') && precision == 0
		&& (unsigned int)list->arg == 0)
		arg = ft_strsub_free(arg, ft_strlen_pro(arg), ft_strlen_pro(arg));
	if (list->special != NULL && ft_strchr(list->special, '-') != NULL)
		padd_digit(&list, arg, field, 1);
	else
		padd_digit(&list, arg, field, 0);
}
