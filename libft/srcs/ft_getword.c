/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getword.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:39 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:13:00 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_getword(char *str, int words, char sep)
{
	int		i;
	int		j;
	int		k;
	char	*word;

	j = ft_countword(str, 1, words, sep);
	i = j;
	k = 0;
	while (str[i] && str[i] != sep)
		i++;
	if (!(word = malloc(sizeof(char) * (i - j))))
		return (0);
	while (j < i)
	{
		word[k] = str[j];
		k++;
		j++;
	}
	word[k] = '\0';
	return (word);
}
