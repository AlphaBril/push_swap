/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swallow.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:29:05 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_swallow(int fd, char **line)
{
	int		rd;
	char	buf[BUFF_SIZE + 1];

	rd = 1;
	while (rd != 0)
	{
		if ((rd = read(fd, buf, BUFF_SIZE)) == -1)
			return (-1);
		buf[rd] = '\0';
		if (!(*line = ft_strjoin_free(*line, buf, 1)))
			return (-1);
	}
	return (0);
}
