/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_countword.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:39 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:10:48 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_countword(char *str, int mode, int word, char sep)
{
	int i;
	int count;

	i = 0;
	count = -1;
	if (str)
	{
		while (str[i])
		{
			if (str[i] == sep)
				i++;
			else
			{
				count++;
				if (mode == 1 && count == word)
					return (i);
				while (str[i] != sep && str[i])
					i++;
			}
		}
	}
	return (count + 1);
}
