/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nmatch.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:41 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:37:00 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_nmatch(char *s1, char *s2)
{
	if (s1 && s2)
	{
		if (*s1 == '\0' && *s2 == '\0')
			return (1);
		if (*s2 == '*')
		{
			if (*s1 == '\0')
				return (ft_nmatch(s1, s2 + 1));
			else
				return (ft_nmatch(s1, s2 + 1) + ft_nmatch(s1 + 1, s2));
		}
		if (*s1 == '\0' || *s2 == '\0')
			return (0);
		if (*s1 != *s2)
			return (0);
		if (*s1 == *s2)
			return (ft_nmatch(s1 + 1, s2 + 1));
	}
	return (0);
}
