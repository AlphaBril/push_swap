/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_tools.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:40 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:16:03 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_lprintf	*ft_create_argument_list(void)
{
	t_lprintf	*new;

	if (!(new = (t_lprintf*)malloc(sizeof(t_lprintf))))
		return (NULL);
	new->special = NULL;
	new->field = NULL;
	new->precision = NULL;
	new->dig_pos_pre = NULL;
	new->modifier = NULL;
	new->conversion = NULL;
	new->percent = NULL;
	new->arg = NULL;
	new->to_swap = NULL;
	new->next = NULL;
	new->fd = 1;
	new->error = 0;
	new->f = 0;
	new->lf = 0;
	return (new);
}

void		ft_list_del(t_lprintf **list)
{
	(*list)->special ? ft_strdel(&(*list)->special) : NULL;
	(*list)->field ? ft_strdel(&(*list)->field) : NULL;
	(*list)->precision ? ft_strdel(&(*list)->precision) : NULL;
	(*list)->dig_pos_pre ? ft_strdel(&(*list)->dig_pos_pre) : NULL;
	(*list)->modifier ? ft_strdel(&(*list)->modifier) : NULL;
	(*list)->conversion ? ft_strdel(&(*list)->conversion) : NULL;
	(*list)->percent ? ft_strdel(&(*list)->percent) : NULL;
	(*list)->to_swap ? ft_strdel(&(*list)->to_swap) : NULL;
	(*list)->fd = 0;
	(*list)->error = 0;
	(*list)->f = 0;
	(*list)->lf = 0;
}

void		ft_lst_del(t_lprintf **list)
{
	while ((*list))
	{
		ft_list_del((t_lprintf**)&*list);
		free(*list);
		*list = (*list)->next;
	}
}
