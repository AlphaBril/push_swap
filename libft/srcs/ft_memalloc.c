/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:40 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:18:03 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memalloc(size_t size)
{
	size_t			i;
	void			*my_alloc;
	unsigned char	*temp;

	if (!(my_alloc = (void*)malloc(sizeof(void) * size)))
		return (NULL);
	i = 0;
	temp = (unsigned char*)my_alloc;
	while (i < size)
	{
		temp[i] = 0;
		i++;
	}
	return (my_alloc);
}
