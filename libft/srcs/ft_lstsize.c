/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstsize.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:40 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:17:14 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_lstsize(t_list *alst)
{
	t_list	*tmp;
	int		i;

	i = 0;
	tmp = alst;
	while (tmp)
	{
		tmp = tmp->next;
		i++;
	}
	return (i);
}
