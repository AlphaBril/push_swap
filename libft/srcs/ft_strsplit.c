/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:28:09 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	**ft_strsplit(char const *s, char c)
{
	int		i;
	int		words;
	char	**ret;

	if (s)
	{
		i = 0;
		words = ft_countword((char*)s, 0, 0, c);
		if (!(ret = (char**)malloc(sizeof(char*) * (words + 1))))
			return (NULL);
		while (i < words)
		{
			ret[i] = ft_getword((char*)s, i, c);
			i++;
		}
		ret[i] = 0;
		return (ret);
	}
	return (NULL);
}
