/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_unsigned.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:44 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:21:55 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static char	*compose_arg(t_lprintf **list, unsigned long long arg,
	int precision, int field)
{
	int		i;
	char	*ret;
	char	*arglength;

	i = 0;
	ret = NULL;
	arglength = ft_utoa_long(arg);
	if (ft_strchr((*list)->special, '0') != NULL && precision == 0
		&& ft_strchr((*list)->special, '-') == NULL)
		while (i++ < field - (int)ft_strlen(arglength))
			ret = ft_strjoin_free(ret, "0", 1);
	else if (precision > 0)
		while (i++ < precision - (int)ft_strlen(arglength))
			ret = ft_strjoin_free(ret, "0", 1);
	ret = ft_strjoin_free(ret, ft_utoa_long(arg), 0);
	ft_strdel(&arglength);
	return (ret);
}

void		handle_unsigned(t_lprintf *list)
{
	int		precision;
	int		field;
	char	*arg;

	precision = list->dig_pos_pre != NULL ? ft_atoi(list->dig_pos_pre) : 0;
	field = list->field != NULL ? ft_atoi(list->field) : 0;
	if (list->modifier)
	{
		if (ft_match(list->modifier, "l"))
			arg = compose_arg(&list,
					(unsigned long)list->arg, precision, field);
		else if (ft_match(list->modifier, "ll"))
			arg = compose_arg(&list,
					(unsigned long long)list->arg, precision, field);
		else if (ft_match(list->modifier, "z"))
			arg = compose_arg(&list, (ssize_t)list->arg, precision, field);
		else if (ft_match(list->modifier, "j"))
			arg = compose_arg(&list, (uintmax_t)list->arg, precision, field);
		else
			arg = compose_arg(&list, (unsigned int)list->arg, precision, field);
	}
	else
		arg = compose_arg(&list, (unsigned int)list->arg, precision, field);
	padd_digit(&list, arg, field,
		list->special != NULL && ft_strchr(list->special, '-') != NULL);
}
