/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:43 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:27:54 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void		ft_strrev(char *s)
{
	size_t	i;
	int		j;
	char	*begin;
	char	*end;
	char	temp;

	begin = s;
	end = s;
	i = 0;
	j = ft_strlen(s) - 1;
	while (i < ft_strlen(s) / 2)
	{
		temp = end[j];
		end[j] = begin[i];
		begin[i] = temp;
		i++;
		j--;
	}
}
