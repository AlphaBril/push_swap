/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:39 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/30 19:15:15 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int			ft_init(char *percent, t_lprintf **list, void *arg)
{
	t_lprintf *tmp;

	if (*list)
	{
		tmp = *list;
		while (tmp && tmp->next)
			tmp = tmp->next;
		tmp->next = ft_create_argument_list();
		tmp = tmp->next;
	}
	else
		tmp = ft_create_argument_list();
	tmp->percent = percent;
	tmp->arg = arg;
	if (!*list)
		*list = tmp;
	return (0);
}

int			ft_init_float(char *percent, t_lprintf **list, double arg)
{
	t_lprintf *tmp;

	if (*list)
	{
		tmp = *list;
		while (tmp && tmp->next)
			tmp = tmp->next;
		tmp->next = ft_create_argument_list();
		tmp = tmp->next;
	}
	else
		tmp = ft_create_argument_list();
	tmp->percent = percent;
	tmp->f = arg;
	if (!*list)
		*list = tmp;
	return (0);
}

int			ft_init_float_long(char *percent, t_lprintf **list, long double arg)
{
	t_lprintf *tmp;

	if (*list)
	{
		tmp = *list;
		while (tmp && tmp->next)
			tmp = tmp->next;
		tmp->next = ft_create_argument_list();
		tmp = tmp->next;
	}
	else
		tmp = ft_create_argument_list();
	tmp->percent = percent;
	tmp->lf = arg;
	if (!*list)
		*list = tmp;
	return (0);
}
