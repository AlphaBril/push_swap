/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoll.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:38 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/30 19:14:29 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int	ft_isspace(char c)
{
	return (c == ' ' || c == '\f' || c == '\t' || c == '\n'
		|| c == '\r' || c == '\v');
}

long long	ft_atoll(const char *str)
{
	int			i;
	long long	ret;
	int			neg;

	i = 0;
	ret = 0;
	while (ft_isspace(str[i]))
		i++;
	neg = (str[i] == '-') ? -1 : 1;
	(str[i] == '-' || str[i] == '+') ? i++ : 0;
	while (ft_isdigit((int)str[i]))
		ret = ret * 10 + (str[i++] - '0');
	return (ret * neg);
}
