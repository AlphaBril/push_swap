/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ldtob.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:40 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:15:50 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_ldtob(long double val)
{
	char			*ret;
	unsigned		idx;
	unsigned char	*ptr;

	ptr = (unsigned char*)&val;
	idx = 8 * sizeof(val);
	ret = NULL;
	while (idx)
	{
		ret = ptr[idx / 8] & (1u << (idx % 8))
			? ft_strjoin_free(ret, "1", 1) : ft_strjoin_free(ret, "0", 1);
		idx--;
	}
	return (ret);
}
