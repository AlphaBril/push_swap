/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen_free.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:42 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:25:10 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

size_t	ft_strlen_free(char *s)
{
	int i;

	i = 0;
	if (s && s[i])
		while (s[i])
			i++;
	if (s)
		ft_strdel(&s);
	return (i);
}
