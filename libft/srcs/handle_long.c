/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_long.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 18:25:44 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 16:17:45 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static char	*compose_arg(t_lprintf **list, long long int arg,
	int precision, int field)
{
	int		i;
	char	*ret;
	char	*arglength;

	i = 0;
	ret = NULL;
	arglength = ft_itoa_long(arg);
	if (ft_strchr((*list)->special, '0') != NULL && precision == 0
		&& ft_strchr((*list)->special, '-') == NULL)
		while (i++ < field - (int)ft_strlen(arglength))
			ret = ft_strjoin_free(ret, "0", 1);
	else if (precision > 0)
		while (i++ < precision - (int)ft_strlen(arglength))
			ret = ft_strjoin_free(ret, "0", 1);
	ret = ft_strjoin_free(ret, arglength, 1);
	ft_strdel(&arglength);
	return (ret);
}

void		handle_long(t_lprintf *list)
{
	int				precision;
	int				field;
	char			*arg;

	precision = list->dig_pos_pre != NULL ? ft_atoi(list->dig_pos_pre) : 0;
	field = list->field != NULL ? ft_atoi(list->field) : 0;
	if (list->modifier && ft_match(list->modifier, "j"))
		arg = compose_arg(&list, (intmax_t)list->arg, precision, field);
	else
		arg = compose_arg(&list, (long long int)list->arg, precision, field);
	if (list->special != NULL && ft_strchr(list->special, '-') != NULL)
		padd_digit(&list, arg, field, 1);
	else
		padd_digit(&list, arg, field, 0);
}
