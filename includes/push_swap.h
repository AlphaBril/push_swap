/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fldoucet <fldoucet@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 16:08:24 by fldoucet          #+#    #+#             */
/*   Updated: 2019/04/19 15:04:37 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "../libft/includes/libft.h"
# include "mlx.h"

typedef struct	s_push
{
	char			*actio;
	struct s_push	*next;
}				t_push;

typedef struct	s_pile
{
	long long		val;
	struct s_pile	*next;
}				t_pile;

typedef struct	s_mlx
{
	void			*init;
	void			*win;
	void			*image;
	char			*data;
	char			*action;
	int				pause;
	int				speed;
	int				ymax;
	int				xmax;
	int				nbr;
	t_push			*tmp;
	t_push			*prev;
	t_push			**start;
	t_pile			**a;
	t_pile			**b;
	void			*gg;
	void			*fl;
	int				col_bac;
	int				cl_ps;
	int				cl_ng;
}				t_mlx;

void			ft_error(int error);
void			ft_lst_push_swap(t_push **swap, t_push *new);
void			ft_lst_push_pile(t_pile **swap, t_pile *new);
void			ft_print_instruct(t_push **swap);
void			ft_print_piles(t_pile *a, t_pile *b);
void			ft_check_instruct(t_push **swap, t_pile **a);
void			ft_exec(t_push **swap, t_pile **a, t_pile **b, int visu);
void			ft_pile_swap(t_pile **a, t_pile **b, int mode);
void			ft_pile_push(t_pile **a, t_pile **b, int mode);
void			ft_pile_rotate(t_pile **a, t_pile **b, int mode);
void			ft_pile_rev_rotate(t_pile **a, t_pile **b, int mode);
void			ft_quicksort(t_pile **a, t_pile **b, t_pile **pivot,
	t_push **swap);
void			ft_check_swap(t_push **swap, int mode);
void			ft_push_swap(t_push **swap, char *str);
void			ft_push_ope(t_pile **a, t_pile **b, t_push **swap, char *actio);
void			ft_im_free(t_pile **a, t_pile **b, t_pile **pivot,
	t_push **swap);
void			ft_draw_piles(t_pile **a, t_pile **b, t_mlx **new, int end);
void			pixel_put(char *str, int x, int y, int color);
void			ft_init_mlx(t_mlx **new, t_pile **a, t_pile **b, t_push **swap);
void			ft_draw_interface(t_mlx **new);
void			ft_draw_text(t_mlx **new, int mode);
void			ft_draw_str(t_mlx **new);
void			ft_set_color(int key, t_mlx **new);
void			ft_last_sort(t_pile **a, t_pile **b, t_pile **pivot,
	t_push **swap);
void			ft_del_swap(t_push **swap);
void			ft_op(char *str, t_pile **a, t_pile **b);
void			ft_free_pile(t_pile **to_free);
void			ft_check_sab(t_pile **a, t_pile **b, t_push **swap);
void			ft_rev_op(char *str, t_pile **a, t_pile **b);
void			ft_rembobinate(t_mlx **new);
void			ft_pause(t_mlx **new);
void			ft_check_double(t_pile **a);
void			ft_min_max(t_pile **a, t_pile **b, t_push **swap);
void			ft_select_sort(t_pile **a, t_pile **b, t_push **swap);
int				ft_brute_sort(t_pile **a, t_pile **b, t_push **swap);
int				ft_check_list(t_pile **a, t_pile **b, t_push **swap);
int				ft_pile_size(t_pile **to_size);
int				ft_swap_size(t_push **to_size);
int				ft_mediane(t_pile **a, int mode);
int				ft_to_sort(t_pile **a, int pivot);
int				ft_next_down(t_pile **a, int pivot);
int				ft_next_up(t_pile **a, int pivot);
int				ft_pivot_down(t_pile **a, int pivot);
int				ft_smallnext(t_pile **b);
int				ft_check_pile(t_pile **a);
int				ft_smaller(t_pile **a);
int				ft_worth_double(t_pile **b);
int				ft_worth_rev_double(t_pile **b);
int				deal_key(int key, void *arg);
int				ft_pile_max(t_pile **a);
int				ft_pixel_heigth(t_pile **a, t_mlx **new);
int				ft_min(t_pile **a);
int				ft_to_sortb(t_pile **b, int pivot);
int				ft_socket_size(t_pile **socket, int end);
int				ft_socket_enda(t_pile **socket);
int				ft_last_piv(t_pile **pivot, t_pile **b, int check);
int				ft_check_destroy(t_push *tmp);
int				ft_check_replacer(t_push *tmp);
int				ft_check_replacerr(t_push *tmp);

#endif
